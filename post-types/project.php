<?php

/**
 * Registers the `project` post type.
 */
function project_init() {
	register_post_type( 'project', array(
		'labels'                => array(
			'name'                  => __( 'Projekte', 'gabriele' ),
			'singular_name'         => __( 'Projekt', 'gabriele' ),
			'all_items'             => __( 'Alle Projekte', 'gabriele' ),
			'archives'              => __( 'Projekt Archive', 'gabriele' ),
			'attributes'            => __( 'Projekt Attributen', 'gabriele' ),
			'insert_into_item'      => __( 'In Projekt einfügen', 'gabriele' ),
			'uploaded_to_this_item' => __( 'Hochgeladen zu diesem Projekt', 'gabriele' ),
			'featured_image'        => _x( 'Beitragsbild', 'project', 'gabriele' ),
			'set_featured_image'    => _x( 'Beitragsbild auswählen', 'project', 'gabriele' ),
			'remove_featured_image' => _x( 'Beitragsbild entfernen', 'project', 'gabriele' ),
			'use_featured_image'    => _x( 'Als Beitragsbild verwenden', 'project', 'gabriele' ),
			'filter_items_list'     => __( 'Filter Projekt-Liste', 'gabriele' ),
			'items_list_navigation' => __( 'Projekt-Liste Navigation', 'gabriele' ),
			'items_list'            => __( 'Projekt-liste', 'gabriele' ),
			'new_item'              => __( 'Neues Projekt', 'gabriele' ),
			'add_new'               => __( 'Neues', 'gabriele' ),
			'add_new_item'          => __( 'Neues Projekt hinzufügen', 'gabriele' ),
			'edit_item'             => __( 'Projekt anpassen', 'gabriele' ),
			'view_item'             => __( 'Projekt anschauen', 'gabriele' ),
			'view_items'            => __( 'Projekte anschauen', 'gabriele' ),
			'search_items'          => __( 'Projekte suchen', 'gabriele' ),
			'not_found'             => __( 'Keine Projekte gefunden', 'gabriele' ),
			'not_found_in_trash'    => __( 'Keine Projekte gefunden in trash', 'gabriele' ),
			'parent_item_colon'     => __( 'Parent Projekt:', 'gabriele' ),
			'menu_name'             => __( 'Projekte', 'gabriele' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => __('hoerbuecher','gabriele'),
		'query_var'             => true,
		'menu_icon'             => 'dashicons-microphone',
		'show_in_rest'          => true,
		'rest_base'             => 'project',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'taxonomies'              => array( 'post_tag', 'category', 'publisher', 'genre', 'series' ),
	) );

}
add_action( 'init', 'project_init' );

/**
 * Sets the post updated messages for the `project` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `project` post type.
 */
function project_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['project'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Projekt aktualisiert. <a target="_blank" href="%s">Projekt anschauen</a>', 'gabriele' ), esc_url( $permalink ) ),
		2  => __( 'Custom field aktualisiert.', 'gabriele' ),
		3  => __( 'Custom field gelöscht.', 'gabriele' ),
		4  => __( 'Projekt aktualisiert.', 'gabriele' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Projekt restored to revision from %s', 'gabriele' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Projekt publiziert. <a href="%s">View Projekt</a>', 'gabriele' ), esc_url( $permalink ) ),
		7  => __( 'Projekt gespeichert.', 'gabriele' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Projekt submitted. <a target="_blank" href="%s">Preview Projekt</a>', 'gabriele' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Projekt scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Projekt</a>', 'gabriele' ),
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Projekt draft aktualisiert. <a target="_blank" href="%s">Preview Projekt</a>', 'gabriele' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'project_updated_messages' );


function project_filter_get_posts($query) {
    if ( $query->is_admin()
        || ! $query->is_main_query()
    ) {
        return $query;
    }

    if ( $query->is_tag() || $query->is_tax(['genre','publisher','category']) ) {
        $query->set('post_type', array('project'));
    }

    return $query;
}
add_action( 'pre_get_posts', 'project_filter_get_posts' );