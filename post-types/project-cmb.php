<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'gabriele_blum_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @package  Gabriele_Blum_Post_Types
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */

add_action( 'cmb2_admin_init', 'gabriele_blum_register_project_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function gabriele_blum_register_project_metabox() {
	$prefix = 'gabriele_blum_project_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$project_details = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Project Details', 'gabriele-blum' ),
		'object_types'  => array( 'project' ), // Post type
		// 'show_on_cb' => 'gabriele_blum_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'gabriele_blum_add_some_classes', // Add classes through a callback.
	) );

    $project_details->add_field( array(
        'name'            => esc_html__( 'Project Autor', 'gabriele-blum' ),
        'desc'            => esc_html__( 'Wer hat das Buch geschrieben?', 'gabriele-blum' ),
        'id'              => $prefix . 'author',
        'type'            => 'text',
        'sanitization_cb' => 'gabriele_blum_sanitize_project_author',
    ) );

	$project_details->add_field( array(
		'name'            => esc_html__( 'Project URL', 'gabriele-blum' ),
		'desc'            => esc_html__( 'Link zu der Webseite beim Verlag', 'gabriele-blum' ),
		'id'              => $prefix . 'url',
		'type'            => 'text',
		'sanitization_cb' => 'gabriele_blum_sanitize_project_url',
	) );

	$project_details->add_field( array(
		'name'            => esc_html__( 'Spieldauer', 'gabriele-blum' ),
		'desc'            => esc_html__( '** Std. und ** Min. (mit/ohne Hinweis in Klammern)', 'gabriele-blum' ),
		'id'              => $prefix . 'duration',
		'type'            => 'text',
	) );

    $project_details->add_field( array(
        'name'            => esc_html__( 'Sprecher', 'gabriele-blum' ),
        'desc'            => esc_html__( 'Namen von (weiteren) Sprechern bei diesem Buch', 'gabriele-blum' ),
        'id'              => $prefix . 'actors',
        'type'            => 'text',
    ) );

    $project_details->add_field( array(
        'name'            => esc_html__( 'Series number', 'gabriele-blum' ),
        'desc'            => esc_html__( 'Wenn es eine Serie betrifft, schreib hier dann die Folgenummer rein', 'gabriele-blum' ),
        'id'              => $prefix . 'series_number',
        'type'            => 'text',
        'sanitization_cb' => 'gabriele_blum_sanitize_project_series_number',
    ) );
}

function gabriele_blum_sanitize_project_url( $url ) {
	return esc_url_raw( $url );
}
