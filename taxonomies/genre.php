<?php

/**
 * Registers the `genre` taxonomy,
 * for use with 'project'.
 */
function genre_init() {
	register_taxonomy( 'genre', array( 'project' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Genres', 'gabriele-blum' ),
			'singular_name'              => _x( 'Genre', 'taxonomy general name', 'gabriele-blum' ),
			'search_items'               => __( 'Search Genres', 'gabriele-blum' ),
			'popular_items'              => __( 'Popular Genres', 'gabriele-blum' ),
			'all_items'                  => __( 'All Genres', 'gabriele-blum' ),
			'parent_item'                => __( 'Parent Genre', 'gabriele-blum' ),
			'parent_item_colon'          => __( 'Parent Genre:', 'gabriele-blum' ),
			'edit_item'                  => __( 'Edit Genre', 'gabriele-blum' ),
			'update_item'                => __( 'Update Genre', 'gabriele-blum' ),
			'view_item'                  => __( 'View Genre', 'gabriele-blum' ),
			'add_new_item'               => __( 'Add New Genre', 'gabriele-blum' ),
			'new_item_name'              => __( 'New Genre', 'gabriele-blum' ),
			'separate_items_with_commas' => __( 'Separate Genres with commas', 'gabriele-blum' ),
			'add_or_remove_items'        => __( 'Add or remove Genres', 'gabriele-blum' ),
			'choose_from_most_used'      => __( 'Choose from the most used Genres', 'gabriele-blum' ),
			'not_found'                  => __( 'No Genres found.', 'gabriele-blum' ),
			'no_terms'                   => __( 'No Genres', 'gabriele-blum' ),
			'menu_name'                  => __( 'Genres', 'gabriele-blum' ),
			'items_list_navigation'      => __( 'Genres list navigation', 'gabriele-blum' ),
			'items_list'                 => __( 'Genres list', 'gabriele-blum' ),
			'most_used'                  => _x( 'Most Used', 'genre', 'gabriele-blum' ),
			'back_to_items'              => __( '&larr; Back to Genres', 'gabriele-blum' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'genre',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'genre_init' );

/**
 * Sets the post updated messages for the `genre` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `genre` taxonomy.
 */
function genre_updated_messages( $messages ) {

	$messages['genre'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Genre added.', 'gabriele-blum' ),
		2 => __( 'Genre deleted.', 'gabriele-blum' ),
		3 => __( 'Genre updated.', 'gabriele-blum' ),
		4 => __( 'Genre not added.', 'gabriele-blum' ),
		5 => __( 'Genre not updated.', 'gabriele-blum' ),
		6 => __( 'Genres deleted.', 'gabriele-blum' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'genre_updated_messages' );
