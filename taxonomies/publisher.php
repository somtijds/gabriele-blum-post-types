<?php

/**
 * Registers the `publisher` taxonomy,
 * for use with 'project'.
 */
function publisher_init() {
	register_taxonomy( 'publisher', array( 'project' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => __('verlag','gabriele'),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Publishers', 'gabriele' ),
			'singular_name'              => _x( 'Publisher', 'taxonomy general name', 'gabriele' ),
			'search_items'               => __( 'Search Publishers', 'gabriele' ),
			'popular_items'              => __( 'Popular Publishers', 'gabriele' ),
			'all_items'                  => __( 'All Publishers', 'gabriele' ),
			'parent_item'                => __( 'Parent Publisher', 'gabriele' ),
			'parent_item_colon'          => __( 'Parent Publisher:', 'gabriele' ),
			'edit_item'                  => __( 'Edit Publisher', 'gabriele' ),
			'update_item'                => __( 'Update Publisher', 'gabriele' ),
			'view_item'                  => __( 'View Publisher', 'gabriele' ),
			'add_new_item'               => __( 'New Publisher', 'gabriele' ),
			'new_item_name'              => __( 'New Publisher', 'gabriele' ),
			'separate_items_with_commas' => __( 'Separate publishers with commas', 'gabriele' ),
			'add_or_remove_items'        => __( 'Add or remove publishers', 'gabriele' ),
			'choose_from_most_used'      => __( 'Choose from the most used publishers', 'gabriele' ),
			'not_found'                  => __( 'No publishers found.', 'gabriele' ),
			'no_terms'                   => __( 'No publishers', 'gabriele' ),
			'menu_name'                  => __( 'Publishers', 'gabriele' ),
			'items_list_navigation'      => __( 'Publishers list navigation', 'gabriele' ),
			'items_list'                 => __( 'Publishers list', 'gabriele' ),
			'most_used'                  => _x( 'Most Used', 'publisher', 'gabriele' ),
			'back_to_items'              => __( '&larr; Back to Publishers', 'gabriele' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'publisher',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'publisher_init' );

/**
 * Sets the post updated messages for the `publisher` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `publisher` taxonomy.
 */
function publisher_updated_messages( $messages ) {

	$messages['publisher'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Publisher added.', 'gabriele' ),
		2 => __( 'Publisher deleted.', 'gabriele' ),
		3 => __( 'Publisher updated.', 'gabriele' ),
		4 => __( 'Publisher not added.', 'gabriele' ),
		5 => __( 'Publisher not updated.', 'gabriele' ),
		6 => __( 'Publishers deleted.', 'gabriele' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'publisher_updated_messages' );
