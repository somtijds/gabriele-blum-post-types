<?php

/**
 * Registers the `series` taxonomy,
 * for use with 'post'.
 */
function series_init() {
	register_taxonomy( 'series', array( 'project' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => __('serien', 'gabriele-blum-post-types'),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Serien', 'gabriele-blum-post-types' ),
			'singular_name'              => _x( 'Serie', 'taxonomy general name', 'gabriele-blum-post-types' ),
			'search_items'               => __( 'Serien suchen', 'gabriele-blum-post-types' ),
			'popular_items'              => __( 'Populäre Serien', 'gabriele-blum-post-types' ),
			'all_items'                  => __( 'Alle Serien', 'gabriele-blum-post-types' ),
			'parent_item'                => __( 'Parent Series', 'gabriele-blum-post-types' ),
			'parent_item_colon'          => __( 'Parent Series:', 'gabriele-blum-post-types' ),
			'edit_item'                  => __( 'Serie anpassen', 'gabriele-blum-post-types' ),
			'update_item'                => __( 'Serie aktualisieren', 'gabriele-blum-post-types' ),
			'view_item'                  => __( 'Serie anzeigen', 'gabriele-blum-post-types' ),
			'add_new_item'               => __( 'Neue Serie hinzufügen', 'gabriele-blum-post-types' ),
			'new_item_name'              => __( 'Neue Serie', 'gabriele-blum-post-types' ),
			'separate_items_with_commas' => __( 'Separate series with commas', 'gabriele-blum-post-types' ),
			'add_or_remove_items'        => __( 'Serien hinzufügen oder löschen', 'gabriele-blum-post-types' ),
			'choose_from_most_used'      => __( 'Choose from the most used series', 'gabriele-blum-post-types' ),
			'not_found'                  => __( 'Keine serien gefunden', 'gabriele-blum-post-types' ),
			'no_terms'                   => __( 'Keine serien', 'gabriele-blum-post-types' ),
			'menu_name'                  => __( 'Serien', 'gabriele-blum-post-types' ),
			'items_list_navigation'      => __( 'Serien-Liste Navigation', 'gabriele-blum-post-types' ),
			'items_list'                 => __( 'Serien-Liste', 'gabriele-blum-post-types' ),
			'most_used'                  => _x( 'Meist-benützte serien', 'series', 'gabriele-blum-post-types' ),
			'back_to_items'              => __( '&larr; Zürück zu den Serien', 'gabriele-blum-post-types' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'series',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'series_init' );

/**
 * Sets the post updated messages for the `series` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `series` taxonomy.
 */
function series_updated_messages( $messages ) {

	$messages['series'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Series added.', 'gabriele-blum-post-types' ),
		2 => __( 'Series deleted.', 'gabriele-blum-post-types' ),
		3 => __( 'Series updated.', 'gabriele-blum-post-types' ),
		4 => __( 'Series not added.', 'gabriele-blum-post-types' ),
		5 => __( 'Series not updated.', 'gabriele-blum-post-types' ),
		6 => __( 'Series deleted.', 'gabriele-blum-post-types' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'series_updated_messages' );
