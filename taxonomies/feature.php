<?php

/**
 * Registers the `feature` taxonomy,
 * for use with 'project'.
 */
function feature_init() {
	register_taxonomy( 'feature', array( 'project' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Features', 'gabriele' ),
			'singular_name'              => _x( 'Feature', 'taxonomy general name', 'gabriele' ),
			'search_items'               => __( 'Search Features', 'gabriele' ),
			'popular_items'              => __( 'Popular Features', 'gabriele' ),
			'all_items'                  => __( 'All Features', 'gabriele' ),
			'parent_item'                => __( 'Parent Feature', 'gabriele' ),
			'parent_item_colon'          => __( 'Parent Feature:', 'gabriele' ),
			'edit_item'                  => __( 'Edit Feature', 'gabriele' ),
			'update_item'                => __( 'Update Feature', 'gabriele' ),
			'view_item'                  => __( 'View Feature', 'gabriele' ),
			'add_new_item'               => __( 'New Feature', 'gabriele' ),
			'new_item_name'              => __( 'New Feature', 'gabriele' ),
			'separate_items_with_commas' => __( 'Separate features with commas', 'gabriele' ),
			'add_or_remove_items'        => __( 'Add or remove features', 'gabriele' ),
			'choose_from_most_used'      => __( 'Choose from the most used features', 'gabriele' ),
			'not_found'                  => __( 'No features found.', 'gabriele' ),
			'no_terms'                   => __( 'No features', 'gabriele' ),
			'menu_name'                  => __( 'Features', 'gabriele' ),
			'items_list_navigation'      => __( 'Features list navigation', 'gabriele' ),
			'items_list'                 => __( 'Features list', 'gabriele' ),
			'most_used'                  => _x( 'Most Used', 'feature', 'gabriele' ),
			'back_to_items'              => __( '&larr; Back to Features', 'gabriele' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'feature',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'feature_init' );

/**
 * Sets the post updated messages for the `feature` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `feature` taxonomy.
 */
function feature_updated_messages( $messages ) {

	$messages['feature'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Feature added.', 'gabriele' ),
		2 => __( 'Feature deleted.', 'gabriele' ),
		3 => __( 'Feature updated.', 'gabriele' ),
		4 => __( 'Feature not added.', 'gabriele' ),
		5 => __( 'Feature not updated.', 'gabriele' ),
		6 => __( 'Features deleted.', 'gabriele' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'feature_updated_messages' );
