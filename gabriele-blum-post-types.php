<?php
/**
 * Plugin Name:     Gabriele Blum Post Types
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     gabriele-blum-post-types
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Gabriele_Blum_Post_Types
 */
namespace Gabriele_Blum;

$plugin_dir_path = plugin_dir_path( __FILE__ );

require_once $plugin_dir_path . '/post-types/project.php';
require_once $plugin_dir_path . '/post-types/project-cmb.php';
require_once $plugin_dir_path . '/taxonomies/publisher.php';
require_once $plugin_dir_path . '/taxonomies/feature.php';
require_once $plugin_dir_path . '/taxonomies/genre.php';
require_once $plugin_dir_path . '/taxonomies/series.php';



